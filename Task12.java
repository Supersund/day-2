import java.util.ArrayList;
public class Task12 {
	public static String repeat(int count, String with) {
    return new String(new char[count]).replace("\0", with);
	}
	public static void main(String[] args) {
		int firstInputNumber = 0;
		int secondInputNumber = 0;
		try {
			firstInputNumber = Integer.parseInt(args[0]);
			secondInputNumber = Integer.parseInt(args[1]);
		}
		catch(Exception e) {
			System.out.println("God damnit, Craig. You were suppose to type two integers as arguments!");
			System.exit(1);
		}
		ArrayList<String> outputArray = new ArrayList<String>();
		if (firstInputNumber < 1 || secondInputNumber<1) {
			System.out.println("Doing this on purpose, are we, Craig?");
			System.exit(1);
		}
		
		outputArray.add(repeat(firstInputNumber, "#"));
		if (firstInputNumber>4 && secondInputNumber>4) {
			outputArray.add("#"+repeat(firstInputNumber-2, " ")+"#");
			outputArray.add("# "+repeat(firstInputNumber-4, "#")+" #");
			for (int i=2; i<secondInputNumber-4; i++) {
				outputArray.add("# #"+repeat(firstInputNumber-6, " ")+"# #");
			}
			if (secondInputNumber>5) outputArray.add("# "+repeat(firstInputNumber-4, "#")+" #");
			outputArray.add("#"+repeat(firstInputNumber-2, " ")+"#");
		}
		else {
			for (int i=1; i<secondInputNumber-1; i++) {
				if (firstInputNumber == 1) outputArray.add("#");
				else outputArray.add("#"+repeat(firstInputNumber-2, " ")+"#");
			}
		}
		
		if (secondInputNumber> 1)outputArray.add(repeat(firstInputNumber, "#"));
		
		for (String line : outputArray) {
			System.out.println(line);
		}
	}
}