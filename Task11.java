import java.util.ArrayList;
public class Task11 {
	public static String repeat(int count, String with) {
    return new String(new char[count]).replace("\0", with);
	}
	public static void main(String[] args) {
		int inputNumber = 0;
		try {
			inputNumber = Integer.parseInt(args[0]);
		}
		catch(Exception e) {
			System.out.println("God damnit, Craig. You were suppose to type a number as argument!");
			System.exit(1);
		}
		ArrayList<String> outputArray = new ArrayList<String>();
		if (inputNumber < 1) {
			System.out.println("Doing this on purpose, are we, Craig?");
			System.exit(1);
		}
		
		outputArray.add(repeat(inputNumber, "#"));
		
		for (int i=1; i<inputNumber-1; i++) {
			outputArray.add("#"+repeat(inputNumber-2, " ")+"#");
		}
		
		outputArray.add(repeat(inputNumber, "#"));
		
		for (String line : outputArray) {
			System.out.println(line);
		}
	}
}