public class Task14 {
	public static void main(String[] args) {
		int weight = 0;
		double height = 0;
		try {
			weight = Integer.parseInt(args[0]);
			height = Double.parseDouble(args[1]);
		}
		catch(Exception e) {
			System.out.println("Craig, you gotta enter two arguments. The first one is the weight in kilograms, the second is the height in meters");
			System.exit(1);
		}
		
		double BMI = weight / (height*height);
		System.out.println(BMI);
		if (BMI < 18.5) System.out.println("You're underweight");
		else if (BMI < 25) System.out.println("You're normal");
		else if (BMI < 30) System.out.println("You're overweight");
		else System.out.println("You're obese, you giant!");
	}
}