import java.util.Arrays;

public class Task13 {

	
	public static String[] contactMatches(String search, String[] contactList) {
		return Arrays.stream(contactList).filter(name -> name.contains(search)).toArray(String[]::new);
	}
	
	public static void main(String[] args) {
		String[] contactList = {"Craig Marais", "Mariah Craigy", "Craigack Craigama", "Donald Craig", "Craigimir Putin"};
		if (args.length == 0) {
			System.out.println("For the last time, Craig! You gotta enter an argument to search in the contact list");
			System.exit(1);
		}
		String[] contactMatchesList = contactMatches(args[0], contactList);
		for (String name : contactMatchesList){
			System.out.println(name);
		}
	}
}